﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    public class RotateBolts : MonoBehaviour
    {
        private bool _rotate = false;
        private LinearDrive LinearDrive;
        Vector3 lastPosition = Vector3.zero;
        private float dispSpeed;

        public bool Rotate
        {
            get
            {
                return _rotate;
            }
            set
            {
                _rotate = value;
            }
        }

        private void FixedUpdate()
        {
            dispSpeed = (((transform.position - lastPosition).magnitude) / Time.deltaTime); //check speed of bolts to adjust speed of rotation
            lastPosition = transform.position;
            
        }
        // Update is called once per frame
        void Update()
        {

            if (_rotate)
            {

                transform.Rotate(Vector3.right * dispSpeed, Space.Self); 
            }
            else
            {
                return;
            }
        }
    }
}
