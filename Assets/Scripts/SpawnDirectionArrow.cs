﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    public class SpawnDirectionArrow : MonoBehaviour
    {
        private bool _posAssigned;
        private GameObject _directionArrow;
        private GameObject _modelPart;
        private GameObject _destinationPoint;
        private bool _hasSpawn;

        private void OnEnable() {

            _hasSpawn = true;
            _destinationPoint = GetComponent<LinearDrive>().Destination.gameObject;
            _modelPart = this.gameObject;
            _posAssigned = false;

        }

        public void Spawn() {
            //show arrow only when dragging model part out (not when grabbing back in or when its position has reached the destination)
            if (_hasSpawn && _modelPart.transform.position != _destinationPoint.transform.position) {

                var centerLocation = Vector3.Lerp(_destinationPoint.transform.position, _modelPart.transform.position, 0.8f);

                _directionArrow = Instantiate(Resources.Load("ArrowWithPivot"), centerLocation, Quaternion.identity) as GameObject;
                _directionArrow.transform.up = _destinationPoint.transform.position - _modelPart.transform.position;
                _posAssigned = true;
                _hasSpawn = false;
            }
        }
        public void Despawn(){
        
                Destroy(_directionArrow);
                _hasSpawn = true;

        }
        void Update() {

            if (_posAssigned) { //wait that position gets assigned
                if (_directionArrow.gameObject != null) { //if is not destroyed

                _directionArrow.transform.position = Vector3.MoveTowards(_directionArrow.transform.position, _destinationPoint.transform.position, 0.01f); 

                }
            }
        }
    }
}