﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Valve.VR.InteractionSystem.Sample
{
    public class ButtonExample : MonoBehaviour
    {
        public HoverButton hoverButton;

        private List<GameObject> models = new List<GameObject>();
        public GameObject _modelHolder;
        private Text _modelNumber;

        private void Start()
        {
            hoverButton.onButtonDown.AddListener(OnButtonDown);
            var parentPrefab = transform.parent;
            _modelNumber = parentPrefab.GetComponentInChildren<Text>();
            _modelNumber.text = "Model 1";

            foreach (Transform child in _modelHolder.transform)
            {
                models.Add(child.gameObject);
            }


        }

        private void OnButtonDown(Hand hand)
        {
            StartCoroutine(SwitchModel());
        }

        private IEnumerator SwitchModel()
        {
            foreach(GameObject modelPart in models) //Look through this object children(in this case 2 ) to switch beetween them and change the text on top of the button to show model number
            {
                if (modelPart.gameObject.activeSelf)
                {
                    modelPart.gameObject.SetActive(false);

                    if (_modelNumber.text == "Model 1")
                    {
                        _modelNumber.text = "Model 2";

                    } else if (_modelNumber.text == "Model 2")
                    {
                        _modelNumber.text = "Model 1";
                    }
                } else{

                    modelPart.gameObject.SetActive(true);
               }
            }

            yield return null;
        }

    }
}