﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Valve.VR.InteractionSystem.Sample
{
    public class MoveTowards : MonoBehaviour
    {
        public Transform DestinationPoint;
        public float SpeedOfExplosion;
        public bool CanExplodeOut;
        public bool CanExplodeIn;

        private Vector3 _startingPosition;
        private ModelExplosion _modelExplosion;
        private TextMeshPro _textDescription;
        private InputTester _inputTester;

    private void OnEnable() {

            CanExplodeOut = false;
            CanExplodeIn = false;
            _modelExplosion = FindObjectOfType<ModelExplosion>();
            _startingPosition = transform.position; //save position of model part to go back to it once explode back in
            _inputTester = FindObjectOfType<InputTester>();
        }
        private void OnDisable() {

            if (DestinationPoint != null) { 
               if( transform.position == DestinationPoint.transform.position) { //Reset exploded model parts to their initial positions

                    transform.position = _startingPosition;
                    foreach (Transform child in transform)
                    {
                        if (child.gameObject.tag == "DescriptionPrefab") //if there is a description prefab destroy it so it will be recreate OnEnable of ModelExplosion.cs
                        {
                            Destroy(child.gameObject);
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                 else
                {
                    return;
                }
            }
            else
            {
                return;
            }

        }
        void Update() {

            if (CanExplodeOut) {

                transform.position = Vector3.MoveTowards(transform.position, DestinationPoint.transform.position, SpeedOfExplosion);

                if (transform.position == DestinationPoint.transform.position) {

                        _textDescription = gameObject.GetComponentInChildren<TMPro.TextMeshPro>();
                    if (_textDescription != null)
                    {
                        _textDescription.enabled = _inputTester.Appear;
                        var textDescriptionContentFitter = gameObject.GetComponentInChildren<ContentSizeFitter>();//hack to fit the text properly when enable it
                        textDescriptionContentFitter.enabled = _inputTester.Appear;
                    }
                    _modelExplosion.ChildNumberToExplodeIn++; //model arrive to destination and ready to explode back in
                    CanExplodeOut = false;
                }
            }

            if (CanExplodeIn) {

                transform.position = Vector3.MoveTowards(transform.position, _startingPosition, SpeedOfExplosion);

                if (transform.position == _startingPosition) {

                    _textDescription = gameObject.GetComponentInChildren<TMPro.TextMeshPro>();
                    if (_textDescription != null)
                    {
                        _textDescription.enabled = _inputTester.Appear;
                        var textDescriptionContentFitter = gameObject.GetComponentInChildren<ContentSizeFitter>();//hack to fit the text properly when enable it
                        textDescriptionContentFitter.enabled = _inputTester.Appear;
                    }
                    _modelExplosion.ChildNumberToExplodeOut++; //model arrive to starting position and ready to explode out
                    CanExplodeIn = false;

                }
            }

        }
    }
}
