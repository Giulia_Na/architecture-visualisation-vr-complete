﻿using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace Valve.VR.InteractionSystem.Sample
{
    public class InputTester : MonoBehaviour
    {

        public SteamVR_Action_Boolean TriggerClick, MenuClick;//set in Inspector

        private SteamVR_Input_Sources inputSource;
        private InteractableExample _interactableExample;
        private ModelExplosion _modelExplosion;
        private bool _explodeOut; //Explode model from starting point to destination point
        private bool _explodeIn;  //Explode model from destination point to starting point
        private bool _appear = true; //trigger to show/hide text info
        public bool Appear
        {
            get { return _appear; }
            set
            {
                _appear = value;
            }
        }
        private void OnEnable()
        {
            _interactableExample = FindObjectOfType<InteractableExample>();
            _modelExplosion = FindObjectOfType<ModelExplosion>();
            TriggerClick.AddOnStateDownListener(PressTrigger, inputSource);
            MenuClick.AddOnStateDownListener(PressMenu, inputSource);
        }

        private void OnDisable()
        {
            TriggerClick.RemoveOnStateDownListener(PressTrigger, inputSource);
            MenuClick.RemoveOnStateDownListener(PressMenu, inputSource);
        }

        private void PressTrigger(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)  
        {
            
            if (_interactableExample != null && _interactableExample.IsHandOver) {

                if (_modelExplosion.ExplodeOut) {
                    _modelExplosion.CallExplodeOut = true;
                    _interactableExample.Explode = true;// Update the hint text to be "explode out"
                }
                if (_modelExplosion.ExplodeIn) {
                    _modelExplosion.CallExplodeIn = true;
                    _interactableExample.Explode = false;//Update the hint to be "explode in"
                }

            } else {

                return;
            }
        }

        private void PressMenu(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
        {
            _appear = !_appear; //show or hide texts



        }
        private void Update()
        {
            ShowOrHideText(); 
        }

        public void ShowOrHideText()
        {

            foreach (Transform modelType in transform)
            {
                if (modelType.gameObject.activeSelf) 
                {
                    var allText = modelType.gameObject.GetComponentsInChildren<TMPro.TextMeshPro>(true);

                    foreach (TMPro.TextMeshPro text in allText)
                    {
                        text.gameObject.SetActive(_appear);

                    }
                    }
                }
            }
                     
    }
}