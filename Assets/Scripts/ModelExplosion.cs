﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

namespace Valve.VR.InteractionSystem.Sample {
    public class ModelExplosion : MonoBehaviour {
        public Transform[] DestinationPoints;
        public GameObject[] ModelParts;
        [TextArea] //so it is possible to write with spaces and add paragraphs
        public string[] ModelPartDescription;
        public GameObject DescriptionPrefab;
        public float SpeedOfExplosion;

        private InteractableExample _interactableExample;

        private bool _explodeOut;
        private bool _callExplodeOut;
        private bool _callExplodeIn;
        private bool _explodeIn;
        private int _childNumberToExplodeIn;
        private int _childNumberToExplodeOut;

        public bool ExplodeOut {
            get { return _explodeOut; }
            set { _explodeOut = value; }
        }

        public bool CallExplodeOut {
            get { return _callExplodeOut; }
            set { _callExplodeOut = value; }
        }

        public bool CallExplodeIn {
            get { return _callExplodeIn; }
            set { _callExplodeIn = value; }

        }

        public bool ExplodeIn {
            get { return _explodeIn; }
            set { _explodeIn = value; }

        }

        public int ChildNumberToExplodeIn
        {
            get { return _childNumberToExplodeIn; }
            set { _childNumberToExplodeIn = value; }

        }

        public int ChildNumberToExplodeOut
        {
            get { return _childNumberToExplodeOut; }
            set { _childNumberToExplodeOut = value; }

        }




        private void OnEnable() {

            _explodeIn = false;
            _explodeOut = true;
            _childNumberToExplodeIn = 0;
            _interactableExample = GetComponent<InteractableExample>();
            _interactableExample.Explode = false;

            for (int i = 0; i < ModelParts.Length; i++)
            {
                Vector3 pos = new Vector3(0, 0, 0);
                Vector3 rot = new Vector3(0, 0, 0);
                GameObject instantietedDescription = Instantiate(DescriptionPrefab, transform.position, transform.rotation) as GameObject;
                instantietedDescription.transform.parent = ModelParts[i].transform;
                instantietedDescription.transform.localPosition = pos;
                instantietedDescription.transform.localEulerAngles = rot;
                var textDescription = instantietedDescription.GetComponentInChildren<TMPro.TextMeshPro>();
                textDescription.text = ModelPartDescription[i];
                textDescription.enabled = false;
                var textDescriptionContentFitter = instantietedDescription.GetComponentInChildren<ContentSizeFitter>();//hack to fit the text properly when enable it
                textDescriptionContentFitter.enabled = false;
                ModelParts[i].AddComponent<MoveTowards>();
            }
            }




        void Update() {

            if (_callExplodeOut) { //called in InputTester.cs
                _explodeOut = false;
                _callExplodeOut = false;

                for (int i = 0; i < ModelParts.Length; i++) {
                    var moveTowards = ModelParts[i].GetComponent<MoveTowards>();
                    moveTowards.DestinationPoint = DestinationPoints[i];
                    moveTowards.SpeedOfExplosion = SpeedOfExplosion;
                    moveTowards.CanExplodeOut = true;
                }
            }

            /*in MoveTowards.cs check when all model parts arrive to destination points by
            in MoveTowards.cs check when all model parts arrive to destination points by
            while user press the trigger multiple time*/
            if (_childNumberToExplodeIn == ModelParts.Length) { 
        
                _explodeIn = true;
                _childNumberToExplodeIn = 0;
            }

            if (_callExplodeIn) {//called in InputTester.cs

                _explodeIn = false;
                _callExplodeIn = false;

                for (int i = 0; i < ModelParts.Length; i++) {
                    var moveTowards = ModelParts[i].GetComponent<MoveTowards>();
                    moveTowards.SpeedOfExplosion = SpeedOfExplosion;
                    moveTowards.CanExplodeIn = true;
                }
            }

            /*in MoveTowards.cs check when all model parts arrive to destination points by
            in MoveTowards.cs check when all model parts arrive to destination points by
            while user press the trigger multiple time*/
            if (_childNumberToExplodeOut == ModelParts.Length) { 
                                                                 
                _explodeOut = true;
                _childNumberToExplodeOut = 0;
            }


        }
    }
}
