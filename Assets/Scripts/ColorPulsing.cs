﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPulsing : MonoBehaviour
{
    public float FadeDuration = 1f;
    public Color Color1 = Color.gray;
    public Color Color2 = Color.white;

    private Color _startColor;
    private Color _endColor;
    private float _lastColorChangeTime;
    private Material _material;

    void Start() {
        _material = GetComponent<Renderer>().material;
        _startColor = Color1;
        _endColor = Color2;
    }

    void Update() {

        var ratio = (Time.time - _lastColorChangeTime) / FadeDuration;
        ratio = Mathf.Clamp01(ratio);
        _material.color = Color.Lerp(_startColor, _endColor, ratio);
       // material.color = Color.Lerp(startColor, endColor, Mathf.Sqrt(ratio)); // A cool effect
       //material.color = Color.Lerp(startColor, endColor, ratio * ratio); // Another cool effect

        if (ratio == 1f) {

            _lastColorChangeTime = Time.time;
            // Switch colors
            var temp = _startColor;
            _startColor = _endColor;
            _endColor = temp;
        }
    }
}