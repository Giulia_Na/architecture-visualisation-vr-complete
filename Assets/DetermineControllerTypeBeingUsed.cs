﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Valve.VR;
using UnityEngine.XR;
public class DetermineControllerTypeBeingUsed : MonoBehaviour
{
    [SerializeField]
    private Material OculusInstructions,ViveInstructions;

    [SerializeField]
    private bool UsingOculus = false;

    [SerializeField]
    private uint deviceIndex = 0;

    private void Update()
    {
        if (XRDevice.model == "Oculus Rift CV1")
        {
            UsingOculus = true;
        }
        else
        {
            UsingOculus = false;
        }

        if (UsingOculus)
        {
            GetComponent<Renderer>().material = OculusInstructions;
        }
        else
        {
            GetComponent<Renderer>().material = ViveInstructions;

        }
    }
}
