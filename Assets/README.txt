1. Drag the model in the Inspector
2. Make it as a child of the Model to Explode under Place Models Here gameObject, rest position to 0,0,0 
3. Drag the same model in the Inspector and make it as a child of the Model to Drag, reset its position to 0,0,0 
3. Duplicate it and rename it "ModelToDrag"

For the model to explode 
1. Create a game object name Part1(and so on), drag it at the centre of the part you need to explode and make the componets child of it 
2. fill out the Model explosion script with the parts to explode and the destinations, add also the description
3. change the colliders on the Model to Explode so they cover the shape of your model

To create rotating bolts (do this steps from 1 to 4 for each close group of bolts you need to drag)
1. go to Assets>Prefabs and drag BoltsToDrag in the Inspector, make it a child of the ModelToDrag 
2. set its position in the centre of the part you d like drag
3. select all the parts to drag and make them children of the BoltsToDrag object
4. Assign a Destination point in the Linear Drive script

To create any other part (do this steps from 1 to 4 for each part you need to drag)
1. go to Assets>Prefabs and drag PartToDrag in the Inspector, make it a child of the ModelToDrag
2. set its position in the centre of the part you d like drag
3. select all the parts to drag and make them children of the PartToDrag object
4. Assign a Destination point in the Linear Drive script


REMEMBER TO MAKE ONE OF THE 2 MODEL DISABLE BEFORE PLAYING